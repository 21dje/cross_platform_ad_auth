# cross_platform_ad_auth

A flutter package using both the packages msal_js and aad_oauth to enable cross-platform authentication for apps designed for both mobile and web.
## Setup

The setup for using this pakcage required changes to be made both to the flutter package itself as well as to your Azure AD configuration

### Flutter Project Setup

Multiple different files need to be modified in your project in order for things to work correctly.

#### AndroidManifest modification

In order for the application to run on Android, a small modification must be made to the `build.gradle` filed located at `android/app/build.gradle`.
```
    defaultConfig {
        applicationId "com.example.example"
        minSdkVersion 16
        targetSdkVersion 28
        versionCode flutterVersionCode.toInteger()
        versionName flutterVersionName
    }
```

You must change `minSdkVersion` from 16 to at least 18. This is necessary for the plugin flutter_secure_Storate, which aad_oAuth uses for storage of authentication information.

#### Launch Config

When testing your application on the web platform, it must be run from a specific port that you can specify in the Azure AD application setup. For example, http://localhost:1833.

In order to get Flutter to run the web server or chrome from that port, you must specify it. There are two ways to do this

##### VS Code

If you're using VS Code, you can add a configuration to your `launch.json` file, like so:
```json
        {
            "program": "client_flutter/lib/main.dart",
            "name": "Chrome Port 1833",
            "request": "launch",
            "type": "dart",
            "args": [
                "-d",
                "chrome",
                "--web-port=1833"
            ]
        },
```
This will cause the program to launch, using chrome, on port 1833. This will also enable you to use VS code's debugging tools.

##### Terminal Setup

It can also be run from the terminal, using commands such as

 - `flutter run -d chrome --web-port=1833`
 - `flutter run -d web-server --web-port=1833`

#### index.html

In your project's web/ folder, there is a file called `index.html`. To the file's header, you must add this line of code:

```html
    <!--MSAL Wrapper-->
    <script src="https://alcdn.msftauth.net/lib/1.3.2/js/msal.min.js"></script>
```

This causes the browser to automatically look for and download the MSAL.js library provided by Microsoft for web authentication using Azure.

### Azure AD Setup

The use of this authentication library requires you to have an application set up in Azure AD. You will need your
 - Client ID (also known as an application ID)
 - Your Tenant ID (also known as your directory ID)
 - Your redirect URL (applicable for mobile and web development)

You must ensure that Azure is set up to treat all clients as public clients, otherwise you may run into issues with PKCE (Proof Key for Code Exchange), an additional security measure for private frontend clients.

In order for this application to work:

 - Your redirect URL must be of the "Mobile and Desktop Appliation" type.
 - The Advanced option to treat it as a public client must be set to true.
 - You must have the application set up to grand ID and Access tokens.

## Using this library

All interaction with this library is done through two streams: one where you add requests, and one where you receive responses.

If anything is still confusing, feel free to look at the project in `example`, where there is a functional implementation of the library.

### Stream Events

These requests and responses are defined in util/events.dart - you must use these to interact with the authentication handlers. In this file, the comments should make the classes relatively self-explanatory.

It is recommended to use a StreamBuilder to operate off of these streams. Every stream will respond with a `Status`, whether is is `LoggedIn`, `LoggedOut`, or `LoggingIn`. 


### Accessing the Handler - InheritedHandler

In order to access the `AuthHandler` from anywhere in the Widget tree, an `InheritedWidget` called `InheritedHandler` is provided for you.

It is reccomended to put this InheritedHandler at the very top of your widget tree, such that it is capable of being accessed from anywhere in the Widget tree.
You must first initialize a handler using the `AbstractHandler` class, and then pass it into the InheritedHandler. In order to do so, you must also create a configuration object.

All of this can (and should) be done in the `void main()` function in your main.dart, like so:

```dart
void main() {
  Config config = Config(
      tenant: "8318285e-1b88-4323-b7dc-XXXXXXXXXXx",
      clientId: "0b5adde4-XXXX-XXXX-XXXX-f6132c7f9933",
      redirect: "http://localhost:1833",
      scopes: [
        "openid",
      ]);
  AbstractHandler handler =
      AbstractHandler.getHandler(withDebug: true, config: config);
  runApp(InheritedHandler(handler, child: MyApp()));
}
```

There are three steps to setting this up, as shown above:

 1. Create a configuration file
 2. initialize a handler using the `AbstractHandler.getHandler()` function, making sure to pass in the config.
 3. Using the `runApp` method, wrapping your app in an `InheritedHandler` widget with the handler.

