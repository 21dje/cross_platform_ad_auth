import 'package:cross_platform_ad_auth/util/misc.dart';
import 'package:flutter/foundation.dart';

///An abtract class (can't be instantiated) that describes an input to the handler. Similar to [ResponseEvent].
///Extended by:
/// - [LoginRequest]
/// - [TokenRequest]
/// - [LogoutRequest]
/// - [StatusRequest]
abstract class RequestEvent {}

///Another abstract class (can't be instantiated) that describes output from the handler. Similar to [RequestEvent].
///Extended by:
/// - [LoggingInEvent]
/// - [LoggedInEvent]
/// - [TokenResponse]
/// - [LoggedOutEvent]
/// - [StatusResponse]
abstract class ResponseEvent {
  ///The current LoginStatus of this handler at the time of the response
  final Status status;

  ResponseEvent(this.status);
}

///An event signalling the handler to begin a login attempt.
///The [isRedirect] field is only applicable on a web instance; on a mobile instance, it is just swallowed. Same with [state]
class LoginRequest extends RequestEvent {
  ///Whether or not the MSAL.js handler should trigger a redirect login (vs a popup login)
  final bool isRedirect;

  ///The current state of the client. This can be used to redirect after login, or whatever you would like.
  final String state;
  LoginRequest({this.isRedirect = true, this.state});
}

///An event sent from the handler to the client, saying that the handler is currently logging in.
///Contains a future that will resolve once logged in.
///Also contains a String representing the accountState
class LoggingInEvent extends ResponseEvent {
  final Future loginFuture;
  final String responseState;
  LoggingInEvent(
      {this.loginFuture, this.responseState, @required Status status})
      : super(status);
}

///An event that informs the client that the handler has finished logging in.
///Also returns a token and a state
class LoggedInEvent extends ResponseEvent {
  final String state;
  final String token;
  final String name;
  final String userName;
  LoggedInEvent(
      {this.state,
      this.token,
      @required Status status,
      this.name,
      this.userName})
      : super(status);
}

///An Event where the client asks for the current LoginStatus of the handler, should lead to a [StatusResponse]
class StatusRequest extends RequestEvent {
  @override
  String toString() {
    return "Generic LoginStatus Request";
  }
}

///An event returned by the handler in response to a [StatusRequest]
class StatusResponse extends ResponseEvent {
  Status status;

  @override
  String toString() => "Status response: $status";

  StatusResponse({@required this.status}) : super(status);
}

///Signals the handler to begin the logout process
class LogoutRequest extends RequestEvent {
  @override
  String toString() => "Logout Request";
}

///Given back to the client once the user has logged out
class LoggedOutEvent extends ResponseEvent {
  LoggedOutEvent(Status status) : super(status);

  @override
  String toString() => "Logged out. Status: $status";
}

///An empty request. When this is sent down the stream, a [TokenRespose] should follow.
class TokenRequest extends RequestEvent {
  @override
  String toString() => "Request for a token.";
}

///A response to a [TokenRequest]. Contains an authentication token that can be used for your own requests.
class TokenResponse extends ResponseEvent {
  final String token;
  String toString() =>
      "Token response. " + token == null ? "is empty." : "is not empty";
  TokenResponse({@required this.token, @required Status status})
      : super(status);
}

///A type of response that should be thrown when something goes wrong.
class ErrorResponse extends ResponseEvent {
  final Exception ex;
  @override
  String toString() => "Error Response: $ex. Status: $status";
  ErrorResponse({@required this.ex, @required Status status}) : super(status);
}

///A class that represents the current state of authentication. It will be sent with any event.
///Contains information on the logged in status as well as the user's name and email.
class Status {
  Status({this.name, this.username, this.loginStatus});

  String name;
  String username;
  LoginStatus loginStatus;
}
