import 'package:flutter/widgets.dart';

///A class designed to supply the authentication parameters to the handler in use
class Config {
  ///The redirect URL for the mobile configuration. For the web configuration, the flutter app must be running from the URL specified.
  ///You don't need to specify this, but it is necessary for mobile authentication.
  final String redirect;

  ///The tenant ID for your application. This should be constant across all of your AD applications.
  final String tenant;

  ///The client ID specific to this AD application. This will be different across all of your applications.
  final String clientId;

  ///The authority from where tokens are granted. This is a URL, typicaly `https://login.microsoftonline.com/[tenant]`
  String authority;

  ///A list of scopes that should be requested during authentication.
  final List<String> scopes;

  ///A constructor for the configuration class
  Config(
      {@required this.tenant,
      @required this.clientId,
      @required this.scopes,
      this.authority,
      this.redirect}) {
    if (authority == null) {
      authority = "https://login.microsoftonline.com/$tenant/";
    }
  }
}
