import 'dart:convert';

enum LoginStatus { LoggedIn, LoggingIn, LoggedOut }

class MiscUtil {
  static Map<String, dynamic> decodeToken(String token) {
    List<String> sections = token.split(".");
    Codec<String, String> stringToBase64 = utf8.fuse(base64);
    String decoded = stringToBase64.decode(sections[1]);
    return jsonDecode(decoded);
  }
}
