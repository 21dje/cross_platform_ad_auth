import 'dart:async';

import 'package:cross_platform_ad_auth/handlers/abstract_handler.dart';
import 'package:cross_platform_ad_auth/util/events.dart';
import 'package:aad_oauth/aad_oauth.dart';
import 'package:aad_oauth/model/config.dart' as AadConfig;
import 'package:cross_platform_ad_auth/util/config.dart' as CPAAConfig;
import 'package:cross_platform_ad_auth/util/misc.dart';

///This handler is specifically designed to work on mobile application using the aad_oauth library.
class AuthHandler extends AbstractHandler {
  ///Whether or not the handler should print messages to the console as it goes along
  final bool _withDebug;

  ///The config defined by cross_platform_ad_auth
  final CPAAConfig.Config _cpaaConfig;

  ///The current [Status] of this Handler
  Status _status = Status(loginStatus: LoginStatus.LoggedOut);

  ///Logic to be run during login. Completely optional. Provided with an accessToken.
  Future<void> Function(String accessToken) _loginLogic;

  ///The [StreamController] containing [ResponseEvent] to facilitate communication with this [AuthHandler]
  StreamController<RequestEvent> _requestStreamController;

  ///The [StreamController] containing [RequestEvent] to facilitate communication with this [AuthHandler]
  StreamController<ResponseEvent> _responseStreamController;

  @override
  Sink<RequestEvent> get requestSink => _requestStreamController.sink;

  @override
  Stream<ResponseEvent> get responseStream => _responseStreamController.stream;

  ///A function that will close both [_responseStreamController] and [_requestStreamController]
  void dispose() {
    _requestStreamController.close();
    _responseStreamController.close();
  }

  ///The instance of [AadOAuth] that is used for this handler
  AadOAuth _oAuth;

  AuthHandler(this._withDebug, this._cpaaConfig, this._loginLogic) {
    _debugPrint("Initializing mobile handler...");
    _debugPrint("Initializing streams...");
    //Initializes the stream controller for requests
    _requestStreamController = new StreamController<RequestEvent>();
    _requestStreamController.stream.listen((event) => _handlerEvent(event));
    //Initializes the stream controller for responses
    _responseStreamController = new StreamController<ResponseEvent>.broadcast();
    _responseStreamController.onListen =
        () => _responseStreamController.add(StatusResponse(status: _status));
    //aad_oauth requires their list of scopes as a space delineated string
    _debugPrint("Turning list of scopes into a string...");
    String scopeList = "";
    //Loops through every scope
    for (String scope in _cpaaConfig.scopes) {
      scopeList = scopeList + scope + " ";
    }
    //Removes the extra whitespace
    scopeList = scopeList.trim();
    _debugPrint("Configuring aad_oauth config...");
    AadConfig.Config aadConfig = AadConfig.Config(_cpaaConfig.tenant,
        _cpaaConfig.clientId, scopeList, _cpaaConfig.redirect);
    _debugPrint("Initializing AadOauth...");
    _oAuth = AadOAuth(aadConfig);
  }

  ///A callback that occurs when a [RequestEvent] is added to the [_requestStreamController]
  void _handlerEvent(RequestEvent event) {
    _debugPrint("handling event...");
    //If the request is to log in
    if (event is LoginRequest) {
      _debugPrint("handling login...");
      _login();
    } else if (event is TokenRequest) {
      _debugPrint("handling token request...");
      _handleTokenRequest();
    } else if (event is LogoutRequest) {
      _debugPrint("handling logout...");
      _logout();
    } else if (event is StatusRequest) {
      _debugPrint("Responding to StatusRequest");
      _responseStreamController.add(StatusResponse(status: _status));
    } else {
      throw UnimplementedError("Unexpected type of RequestEvent received.");
    }
  }

  ///Logs out using [AadOAuth.logout] and adds a [LoggedOutEvent] to [_responseStreamController]
  Future<void> _logout() async {
    await _oAuth.logout();
    _status.loginStatus = LoginStatus.LoggedOut;
    _responseStreamController.add(LoggedOutEvent(_status));
  }

  ///A function that calls [AadOAuth.login] then adds a [LoggedInEvent] to the [_responseStreamController], then runs [_handleTokenRequest]
  Future<void> _login() async {
    _debugPrint("Starting login process...");
    _status.loginStatus = LoginStatus.LoggingIn;
    _responseStreamController.add(LoggingInEvent(status: _status));
    try {
      await _oAuth.login();
      _debugPrint("Logged in. Acquiring token...");
      String token = await _handleTokenRequest(addEvent: false);
      _debugPrint("Token acquired. Running postLoginLogic...");
      Future loginLogic = _loginLogic == null
          ? Future.delayed(Duration.zero)
          : _loginLogic(token);
      await loginLogic;
      Map<String, dynamic> accessTokenMap = MiscUtil.decodeToken(token);
      _status.loginStatus = LoginStatus.LoggedIn;
      _status.name = accessTokenMap["name"];
      _status.username = accessTokenMap["unique_name"];
      _responseStreamController.add(LoggedInEvent(
          token: token,
          name: accessTokenMap["name"],
          userName: accessTokenMap["unique_name"],
          status: _status));
    } catch (ex) {
      _debugPrint("Exception caught: $ex");
      //Catches an error, returns, and adds an error response to the stream.
      _status = Status(loginStatus: LoginStatus.LoggedOut);
      _responseStreamController.add(ErrorResponse(ex: ex, status: _status));
      return;
    }
  }

  ///Fetches a token from [AadOAuth.getAccessToken] then adds a [TokenResponse] to [_responseStreamController], if desired.
  Future<String> _handleTokenRequest({bool addEvent = true}) async {
    _debugPrint("Starting token acquisition...");
    String accessToken = await _oAuth.getAccessToken();
    _debugPrint("Token acquired.");
    if (addEvent) {
      _debugPrint("Adding tokenResponse");
      _responseStreamController
          .add(TokenResponse(token: accessToken, status: _status));
    }
    return accessToken;
  }

  ///A simple helper function that only prints if [_withDebug] is true
  void _debugPrint(String message) {
    if (_withDebug) print("mobile_handler: $message");
  }
}
