import 'package:cross_platform_ad_auth/util/config.dart';
import 'package:cross_platform_ad_auth/util/events.dart';
import 'package:flutter/material.dart';
//This imports the correct MSALHandler based on the platform.
//By default, it will import an unsuppored msalHandler that can be found in unsupported_handler.dart
import 'unsupported_handler.dart'
//If the platform supports `dart.library.io`, a.k.a it is a mobile platform, it will import mobile_handler.dart
    if (dart.library.io) 'mobile_handler.dart'
//If the platform supports `dart.library.html`, a.k.a it is a web platform, it will import web_handler.dart
    if (dart.library.html) 'web_handler.dart';

///An abstract handler that provivdes the basic interface for interacting with the handlers.
abstract class AbstractHandler {
  ///The sink that the client should send requests through to the handler.
  Sink<RequestEvent> get requestSink;

  ///The stream that the client can listen to to receive information from the handler.
  Stream<ResponseEvent> get responseStream;

  static AuthHandler getHandler(
      {bool withDebug = false,
      @required Config config,
      Future Function(String accessToken) loginLogic}) {
    //Ensures the flutter engine is initalized as it should be before returning a handler
    WidgetsFlutterBinding.ensureInitialized();
    if (withDebug) print("abstract_handler: returning handler of some kind");
    return AuthHandler(withDebug, config, loginLogic);
  }
}
