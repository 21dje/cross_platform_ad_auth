import 'package:cross_platform_ad_auth/handlers/abstract_handler.dart';
import 'package:cross_platform_ad_auth/util/events.dart';
import 'package:flutter/widgets.dart';

///An inherited widget that provides access to the appropriate [AuthHandler] from anywhere in the widget tree.
class InheritedHandler extends InheritedWidget {
  ///The [AbstractHandler] that this [InheritedHandler] provides access to.
  final AbstractHandler _handler;

  ///The sink that accepts [RequestEvent]s and sends them on to the [_handler]
  Sink<RequestEvent> get requestSink => _handler.requestSink;

  ///The stream that provides [ResponseEvent]s from the [_handler] to the client.
  Stream<ResponseEvent> get responseStream => _handler.responseStream;

  InheritedHandler(this._handler, {Widget child, Key key})
      : super(child: child, key: key);

  static InheritedHandler of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<InheritedHandler>();
  }

  @override
  bool updateShouldNotify(InheritedHandler oldWidget) => true;
}
