import 'dart:async';

import 'package:cross_platform_ad_auth/handlers/abstract_handler.dart';
import 'package:cross_platform_ad_auth/util/events.dart';
import 'package:cross_platform_ad_auth/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:msal_js/msal_js.dart';
import 'package:cross_platform_ad_auth/util/config.dart' as CPAAConfig;

///This handler is specifically designed to work on mobile application using the aad_oauth library.
class AuthHandler extends AbstractHandler {
  ///Whether or not the handler should print messages to the console as it goes along
  final bool _withDebug;

  ///Logic to be run during login. Completely optional. Provided with an accessToken.
  Future<void> Function(String accessToken) _loginLogic;

  ///The config defined by cross_platform_ad_auth
  final CPAAConfig.Config _cpaaConfig;

  ///The current [Status] of this handler
  Status _status = Status(loginStatus: LoginStatus.LoggedOut);

  ///The [AuthRequest] that just contains the scopes, no specific state or anything.
  AuthRequest _defaultLoginRequest;

  ///The [StreamController] containing [ResponseEvent] to facilitate communication with this [AuthHandler]
  StreamController<RequestEvent> _requestStreamController;

  ///The [StreamController] containing [RequestEvent] to facilitate communication with this [AuthHandler]
  StreamController<ResponseEvent> _responseStreamController;

  ///The [UserAgentApplication] that is used to make requests, etc.
  UserAgentApplication _userAgentApplication;

  ///The [Logger] that is logging all of the messages from authentication
  Logger _logger;

  @override
  Sink<RequestEvent> get requestSink => _requestStreamController.sink;

  @override
  Stream<ResponseEvent> get responseStream => _responseStreamController.stream;

  ///A function that will close both [_responseStreamController] and [_requestStreamController]
  void dispose() {
    _requestStreamController.close();
    _responseStreamController.close();
  }

  ///A constrcutor for this auth handler for the web.
  AuthHandler(this._withDebug, this._cpaaConfig, this._loginLogic) {
    _debugPrint("Initializing web handler...");
    _debugPrint("Initializing streams...");
    //Initializes the stream controller for requests
    _requestStreamController = new StreamController<RequestEvent>();
    _requestStreamController.stream.listen(_handlerEvent);
    //Initializes the stream controller for responses
    _responseStreamController = new StreamController<ResponseEvent>.broadcast();
    _responseStreamController.onListen =
        () => _responseStreamController.add(StatusResponse(status: _status));
    _debugPrint("Initializing MSAL logger...");
    //Creates an MSAL logger
    _logger = Logger(
        _loggerCallback,
        LoggerOptions()
          //Logs everything for the purpose of debug
          ..level = LogLevel.verbose);
    _debugPrint("Initializing userAgentApplication");
    _userAgentApplication = UserAgentApplication(Configuration()
      //Initializes the authoptions
      ..auth = (AuthOptions()
        ..clientId = _cpaaConfig.clientId
        ..authority = _cpaaConfig.authority)
      //Adds the logger to the system
      ..system = (SystemOptions()..logger = _logger));
    _debugPrint("adding redirect callback");
    _userAgentApplication.handleRedirectCallback(_redirectCallback);
    _debugPrint("initializing default login request...");
    _defaultLoginRequest = AuthRequest()..scopes = _cpaaConfig.scopes;
    _debugPrint("Handler initialized!");
  }

  ///A callback that occurs when a [RequestEvent] is added to the [_requestStreamController]
  void _handlerEvent(RequestEvent event) {
    _debugPrint("handling event...");
    //If the request is to log in
    if (event is LoginRequest) {
      _debugPrint("handling login...");
      //If it is a redirect request
      if (event.isRedirect) {
        _debugPrint('handling redirect login...');
        //Creates a new authrequest with the given state
        AuthRequest redirectRequest = _copyAuthRequest(_defaultLoginRequest);
        redirectRequest.state = event.state;
        _userAgentApplication.loginRedirect(_defaultLoginRequest);
      } else {
        _debugPrint("handling popup login...");
        _loginPopup();
      }
    } else if (event is TokenRequest) {
      _debugPrint("handling token request...");
      _handleTokenRequest();
    } else if (event is LogoutRequest) {
      _debugPrint("handling logout...");
      _logout();
    } else if (event is StatusRequest) {
      _debugPrint("Responding to StatusRequest");
      _responseStreamController.add(StatusResponse(status: _status));
    } else {
      throw UnimplementedError("Unexpected type of RequestEvent received.");
    }
  }

  ///Logs the user out
  void _logout() {
    //Logs out with the agentapp
    _userAgentApplication.logout();
    _debugPrint("logged out. Adding event.");
    _status = Status(loginStatus: LoginStatus.LoggedOut);
    _responseStreamController.add(LoggedOutEvent(_status));
  }

  Future<String> _handleTokenRequest({bool addEvent = true}) async {
    AuthResponse tokenResponse;
    try {
      //Tries to acquire the token silently; only works if already logged in.
      _debugPrint("acquiring token silently...");
      tokenResponse =
          await _userAgentApplication.acquireTokenSilent(_defaultLoginRequest);
      if (addEvent)
        //Adds the event if it is told to
        _responseStreamController.add(
            TokenResponse(token: tokenResponse.accessToken, status: _status));
      return tokenResponse.accessToken;
      //If not already logged in, there will be anexception thrown.
    } on InteractionRequiredAuthException {
      try {
        //Tries to acquire with a popup
        _debugPrint("silent acquisition failed. Acquiring with popup...");
        tokenResponse =
            await _userAgentApplication.acquireTokenPopup(_defaultLoginRequest);
        //Adds the event if it needs to
        if (addEvent)
          _responseStreamController.add(
              TokenResponse(token: tokenResponse.accessToken, status: _status));
        return tokenResponse.accessToken;
      } on AuthException catch (ex) {
        print("An error occured: $ex");
      }
    } on AuthException catch (ex) {
      print("An error occured: $ex");
    }
    //If nothing has returned by now, something went wrong.
    return null;
  }

  ///A function that is attached to [_logger] that will spit out messages from the MSAL handler.
  void _loggerCallback(LogLevel level, String message, bool containsPii) {
    _debugPrint("MSAL: [$level] $message");
  }

  ///A simple helper function that only prints if [_withDebug] is true
  void _debugPrint(String message) {
    if (_withDebug) print("web_handler: $message");
  }

  ///The function that is run when a redirect login happens.
  void _redirectCallback(AuthException error, [AuthResponse response]) async {
    try {
      if (error != null) {
        throw error;
      }
      _debugPrint("redirect callback initialized...");
      _status.loginStatus = LoginStatus.LoggingIn;
      //Runs the loginLogic if necessary.
      Future loginFuture = _loginLogic == null
          ? Future.delayed(Duration.zero)
          : _loginLogic(response.accessToken);
      _debugPrint("Logging in event added.");
      //Adds the loggingInEvent
      _responseStreamController.add(LoggingInEvent(
          loginFuture: loginFuture,
          responseState: response.accountState,
          status: _status));
      await loginFuture;
      //Once complete, it requests a token and adds the loggedInEvent.
      _debugPrint("loginLogic complete. Requesting token...");
      String token = await _handleTokenRequest(addEvent: false);
      _debugPrint("Token received. Adding event.");
      _status = Status(
          loginStatus: LoginStatus.LoggedIn,
          name: _userAgentApplication.getAccount().name,
          username: _userAgentApplication.getAccount().userName);
      _responseStreamController.add(LoggedInEvent(
          token: token,
          state: response.accountState,
          name: _userAgentApplication.getAccount().name,
          userName: _userAgentApplication.getAccount().userName,
          status: _status));
    } catch (ex) {
      _debugPrint("Exception caught: $ex");
      _debugPrint("Someone is logged in: " +
          (_userAgentApplication.getAccount() != null).toString());
      if (_userAgentApplication.getAccount() != null) {
        _logout();
      }
      WidgetsFlutterBinding.ensureInitialized();
      _debugPrint("Dropping ErrorResponse");
      _responseStreamController.add(ErrorResponse(ex: ex, status: _status));
    }
  }

  ///Initializes a popupLogin, and adds a [LoggingInEvent] to the [_responseStreamController].
  ///Then calls [_popupLoginProcess]
  void _loginPopup() {
    _debugPrint("popup login initialized...");
    _debugPrint("Adding loggingInEvent");
    _status.loginStatus = LoginStatus.LoggingIn;
    _responseStreamController.add(
        LoggingInEvent(loginFuture: _popupLoginProcess(), status: _status));
  }

  ///More of the popup login logic. called from [_loginPopup]
  Future<void> _popupLoginProcess() async {
    try {
      final response =
          await _userAgentApplication.loginPopup(_defaultLoginRequest);
      _debugPrint('Popup login successful. name: ${response.account.name}');
      _debugPrint("Requesting token...");
      //Requests the token, not adding another event.
      String token = await _handleTokenRequest(addEvent: false);
      _debugPrint("Token acquired.  Running logic...");
      //Runs the logic if there is any, otherwise provides a dummy future
      Future logicFuture = _loginLogic == null
          ? Future.delayed(Duration.zero)
          : _loginLogic(token);
      await logicFuture;
      _debugPrint("Logic completed.");
      _status = Status(
          loginStatus: LoginStatus.LoggedIn,
          name: _userAgentApplication.getAccount().name,
          username: _userAgentApplication.getAccount().userName);
      _responseStreamController.add(LoggedInEvent(
          token: token,
          status: _status,
          userName: _userAgentApplication.getAccount().userName,
          name: _userAgentApplication.getAccount().name));
    } on AuthException catch (ex) {
      _debugPrint('MSAL: ${ex.errorCode}:${ex.errorMessage}');
    }
  }

  ///Copies an [AuthRequest] into a new [AuthRequest]
  AuthRequest _copyAuthRequest(AuthRequest old) {
    return AuthRequest()
      ..account = old.account
      ..authority = old.authority
      ..claimsRequest = old.claimsRequest
      ..correlationId = old.correlationId
      ..extraQueryParameters = old.extraQueryParameters
      ..extraScopesToConsent = old.extraScopesToConsent
      ..forceRefresh = old.forceRefresh
      ..loginHint = old.loginHint
      ..prompt = old.prompt
      ..redirectUri = old.redirectUri
      ..scopes = old.scopes
      ..sid = old.sid
      ..state = old.state;
  }
}
