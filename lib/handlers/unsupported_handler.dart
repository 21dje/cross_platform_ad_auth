import 'package:cross_platform_ad_auth/handlers/abstract_handler.dart';
import 'package:cross_platform_ad_auth/util/config.dart';
import 'package:cross_platform_ad_auth/util/events.dart';

///A type of handler that will only be instantiated on unsupported platforms.
///It doesn't do much but throw errors.
class AuthHandler extends AbstractHandler {
  // ignore: unused_field
  final bool _withDebug;
  // ignore: unused_field
  final Config _cpaaConfig;
  // ignore: unused_field
  final Future Function(String accessToken) _loginLogic;

  @override
  Sink<RequestEvent> get requestSink => throw UnimplementedError();

  @override
  Stream<ResponseEvent> get responseStream => throw UnimplementedError();

  AuthHandler(this._withDebug, this._cpaaConfig, this._loginLogic);
}
