## [1.1.2] - August 25, 2020
Added Doc

## [1.1.1] - August 25, 2020
Modified Status readings on responses
Added some comments

## [1.1.0] - August 24, 2020
Firstly, I changed the format of the version releases because now it just makes more sense.

Added name/email fetching from the access token.

## [1.0.0] - August 21, 2020
Initial release. Allows authentication from web, android, and iOS platforms.