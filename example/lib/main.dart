import 'package:cross_platform_ad_auth/cross_platform_ad_auth.dart';
import 'package:flutter/material.dart';

const bool WITH_DEBUG = true;

void main() {
  Config config = Config(
      tenant: "7dbe9ed1-a8d0-42c5-b4f0-b96dbb32b4ed",
      clientId: "bb82e190-de59-4781-aa09-82dcc7c5d17d",
      redirect: "http://localhost:1833",
      scopes: [
        "openid",
      ]);
  AbstractHandler handler =
      AbstractHandler.getHandler(withDebug: WITH_DEBUG, config: config);
  runApp(InheritedHandler(handler, child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CPAA Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'CPAA Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void showErrorSnackbar(BuildContext context, Exception ex) async {
    _debugPrint("Running error snackbar.");
    Future.delayed(Duration.zero);
    Scaffold.of(context).showSnackBar(
        SnackBar(content: Text("Something went wrong: " + ex.toString())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: StreamBuilder(
              initialData: StatusResponse(
                  status: Status(loginStatus: LoginStatus.LoggedOut)),
              stream: InheritedHandler.of(context).responseStream,
              builder: (context, AsyncSnapshot<ResponseEvent> snapshot) {
                _debugPrint(snapshot.data.toString());
                if (snapshot.data is ErrorResponse) {
                  showErrorSnackbar(
                      context, (snapshot.data as ErrorResponse).ex);
                }
                List<Widget> widgets = new List<Widget>();
                if (snapshot.data.status.loginStatus == LoginStatus.LoggedIn) {
                  String name = (snapshot.data.status.name);
                  String userName = snapshot.data.status.username;
                  widgets.add(Text("Hello, $name ($userName)"));
                }
                switch (snapshot.data.status.loginStatus) {
                  case LoginStatus.LoggedIn:
                    widgets.addAll([
                      Text("You are logged in."),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          RaisedButton(
                            onPressed: () => InheritedHandler.of(context)
                                .requestSink
                                .add(TokenRequest()),
                            child: Text("Request Token"),
                          ),
                          RaisedButton(
                            onPressed: () => InheritedHandler.of(context)
                                .requestSink
                                .add(LogoutRequest()),
                            child: Text("Logout"),
                          ),
                        ],
                      )
                    ]);
                    if (snapshot.data is TokenResponse) {
                      widgets.add(Text((snapshot.data as TokenResponse).token));
                    }
                    break;
                  case LoginStatus.LoggedOut:
                    widgets.addAll([
                      Text("You are not logged in."),
                      RaisedButton(
                        onPressed: () => InheritedHandler.of(context)
                            .requestSink
                            .add(LoginRequest()),
                        child: Text("Log In"),
                      )
                    ]);
                    break;
                  case LoginStatus.LoggingIn:
                    widgets.addAll([CircularProgressIndicator()]);
                    break;
                  default:
                  //do nothing
                }
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: widgets,
                );
              })),
    );
  }
}

void _debugPrint(String message) {
  if (WITH_DEBUG) {
    print("main.dart: $message");
  }
}
